@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Conversation History</div>

                <div class="card-body">
                    <ul>
                        @foreach ($chatGPTResponses as $chatGPTResponse)
                        <li>
                            <strong>{{ $chatGPTResponse->prompt }}:</strong>
                            {{ $chatGPTResponse->response }}
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
