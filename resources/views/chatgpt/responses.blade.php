@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">Saved Responses</div>

                    <div class="card-body">
                        @if ($responses->count())
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Prompt</th>
                                        <th>Response</th>
                                        <th>User</th>
                                        <th>Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($responses as $response)
                                        <tr>
                                            <td>{{ $response->prompt }}</td>
                                            <td>{{ $response->response }}</td>
                                            <td>{{ $response->user ? $response->user->name : 'Unknown' }}</td>
                                            <td>{{ $response->created_at->format('d/m/Y H:i:s') }}</td>
                                            <td>
                                                <form method="POST" action="{{ route('chatgpt.destroy', $response->id) }}">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $responses->links() }}
                        @else
                            <p>No responses found.</p>
                        @endif
                    </div>

                    <div class="card-footer">
                        <a href="{{ route('chatgpt.index') }}" class="btn btn-primary">Back to home</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
